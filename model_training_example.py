import os
import logging

import matplotlib
import pandas as pd

# Data
from ydata_profiling import ProfileReport
from sklearn.datasets import fetch_openml

# Feature engineering
from sklearn.pipeline import Pipeline
from sklearn import preprocessing as sp
from sklearn.compose import ColumnTransformer
from sklearn.impute import SimpleImputer
from sklearn.feature_selection import SelectPercentile, chi2, SelectKBest, SelectFromModel, f_classif, VarianceThreshold

# Models
from xgboost import XGBClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split, GridSearchCV, RepeatedStratifiedKFold, RandomizedSearchCV

# Performance Metrics
import sklearn.metrics as sm
from xgboost import plot_importance
import scikitplot as skplt

# Parameter Tuning
from skopt import BayesSearchCV
from skopt.space import Real, Integer

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(name="mylogger")
matplotlib.use('TkAgg')

pd.set_option('display.max_colwidth', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.max_rows', 500)

# Data
data_path = os.path.join(os.path.expanduser("~"), "Documents", "data")
SAVE_PATH = os.path.join(os.path.expanduser("~"), "Downloads")
df, y = fetch_openml("titanic", version=1, as_frame=True, return_X_y=True)
df = pd.concat([df, y], axis=1)

NUMERIC_COLS = ["age", "fare"]
CATEGORICAL_COLS = ["embarked", "sex", "pclass"]
DATE_COLS = []

# Remember to take care to split before imputing to avoid data leak (leak acrosstime and examples).
X_TRAIN, X_TEST, Y_TRAIN, Y_TEST = train_test_split(
    df.drop(columns="survived"), df["survived"], test_size=0.2, random_state=0)


def quick_stats(df):
    # Output quick useful stats of a dataframe.
    print(f"Shape: {df.shape}")
    print(df.describe(include="all"))
    num_duplicated = df[df.duplicated(keep="first")]
    total_duplicates = sum(df.duplicated() == True)
    duplicate_to_remove = total_duplicates - num_duplicated.shape[0]
    number_cols = df.select_dtypes(include='number').columns.tolist()
    cat_cols = (df.select_dtypes(include='object').columns.tolist() +
                df.select_dtypes(include='category').columns.tolist())
    date_cols = df.select_dtypes(include='datetime').columns.tolist()
    print(f"Duplicates to remove: {duplicate_to_remove}")
    print(f"Null Summary: {pd.isnull(df).sum()}")
    print("Inferred column types: ")
    print(f"numeric: {number_cols}")
    print(f"categorical: {cat_cols}")
    print(f"date: {date_cols}")


def eda_script():
    # Use of y-profiler to produce EDA report.  Automate repetitive EDA analyses.
    eda_report(df.drop(columns=["body", "boat"]))
    quick_stats(df)
    # After reviewing EDA choose these columns for univariate analysis.
    num_cols = ['pclass', 'age', 'sibsp', 'parch', 'fare', 'body']
    cat_cols = ['name', 'ticket', 'cabin', 'home.dest', 'sex', 'embarked']  # boat should be excluded from train
    print(f"Low variance cols: {get_low_variance(df, num_cols, cat_cols, threshold=0.2)}")
    print("Benchmark logistic model selecting features via vnivariate analysis ")
    print(benchmark_logistic_univariate_feature_selection_script(num_cols, cat_cols, k=1))


def eda_report(df):
    # Correlations:
    # numerical to numerical variable: Spearman correlation coefficient
    # categorical to categorical variable: Cramer’s V association coefficient
    # numerical to categorical: Cramer’s V association coefficient with the numerical variable discretized automatically

    profile = ProfileReport(df, title="Profiling Report")
    profile.to_file('data_report.html')
    import webbrowser
    import os
    filename = os.path.join("file:///", os.getcwd(), 'data_report.html')
    webbrowser.open_new_tab(filename)


def xgb_train_script(save_path=None):
    # Example script to train xgb with bayesian hyperparameter search.
    # https://arxiv.org/abs/1603.02754
    # Fitting feature + model pipeline with sklearn.
    transform = get_gbm_preprocessor(NUMERIC_COLS, CATEGORICAL_COLS + ["cabin"])
    xgb = XGBClassifier(objective='binary:logistic')
    pipeline = Pipeline(steps=[("preprocess", transform), ("xgb", xgb)])
    XGB_Y_TRAIN = Y_TRAIN.astype(int)
    XGB_Y_TEST = Y_TEST.astype(int)
    pipeline.fit(X_TRAIN, XGB_Y_TRAIN)

    # Metrics
    logger.info("Writing metric graphs (w/o fine tuning)")
    predictions = pipeline.predict_proba(X_TEST)
    ax_imp = plot_importance(pipeline[1], importance_type="gain")
    ax_roc = skplt.metrics.plot_roc(XGB_Y_TEST, predictions)
    ax_lift = skplt.metrics.plot_lift_curve(XGB_Y_TEST, predictions)
    ax_imp.figure.savefig(os.path.join(SAVE_PATH, "importance_plot.png"))
    ax_roc.figure.savefig(os.path.join(SAVE_PATH, "roc_plot.png"))
    ax_lift.figure.savefig(os.path.join(SAVE_PATH, "lift_plot.png"))

    logger.info("Bayesian Hyperparmeter tuning with Repeated KFold Cross Validation")

    # Bayesian parameter tuning
    # Define ranges of learning rate, tree-specific parameters, then regularization parameters for Bayesian search.
    # https://www.analyticsvidhya.com/blog/2016/03/complete-guide-parameter-tuning-xgboost-with-codes-python/
    params = {
        'xgb__learning_rate': Real(0.01, 1.0, 'uniform'),
        'xgb__max_depth': Integer(2, 12),
        'xgb__subsample': Real(0.1, 1.0, 'uniform'),  # denotes % random for each tree, example sample
        'xgb__colsample_bytree': Real(0.1, 1.0, 'uniform'),  # % of columns i.e. feature subsample used by tree
        'xgb__reg_lambda': Real(1e-9, 100., 'uniform'),  # L2 regularization
        'xgb__reg_alpha': Real(1e-9, 100., 'uniform'),  # L1 regularization
        'xgb__n_estimators': Integer(50, 5000)
    }

    # XG Boost has both feature, and example randomization to prevent over-fitting. Cross-fold is another safe-guard
    # Can be used with BayesSearch
    cv = RepeatedStratifiedKFold(n_splits=2, n_repeats=1, random_state=1)
    search = BayesSearchCV(estimator=pipeline, search_spaces=params, n_jobs=-1, cv=cv, scoring='roc_auc')
    search.fit(X_TRAIN, XGB_Y_TRAIN)
    print(search.best_score_)
    print(search.best_params_)
    best_pipeline = search.best_estimator_
    best_model = best_pipeline[1]
    if save_path:
        logger.info(f"Saving model to json {save_path}")
        best_model.save_model(os.path.join(save_path))
    return best_model


def get_logistic_preprocessor(numeric_features, categorical_features, remainder="drop"):
    numeric_transformer = Pipeline(
        steps=[
            ("power transform", sp.PowerTransformer(method="yeo-johnson", standardize=True)),
            ("imputer", SimpleImputer(strategy="mean"))  # important: do not use before splitting to avoid data leak
        ]
    )
    categorical_transformer = Pipeline(
        steps=[
            ("encoder", sp.OneHotEncoder(handle_unknown="ignore", sparse_output=False, drop='if_binary')),
            ("selector", SelectPercentile(chi2, percentile=50))  # can choose higher frequence cat varibles.
        ]
    )
    transformers = [
        ("num", numeric_transformer, numeric_features),
        ("cat", categorical_transformer, categorical_features),
    ]

    return ColumnTransformer(transformers=transformers, remainder=remainder)


def logistic_script():
    # Example for benchmark purposes
    pr_logistic = get_logistic_preprocessor(NUMERIC_COLS, CATEGORICAL_COLS)
    pr_logistic.set_output(transform='pandas')  # little trick to show pandas df.

    pipeline = Pipeline(steps=[("preprocess", pr_logistic), ("logistic", LogisticRegression())])
    pipeline.fit(X_TRAIN, Y_TRAIN)
    pipeline.score(X_TEST, Y_TEST)

    pipeline.get_params().keys()

    param_grid = {
        'logistic__penalty': ['l1', 'l2'],
        'logistic__C': [0.1, 1, 10, 100],
        'logistic__solver': ["saga"]
    }
    clf = GridSearchCV(pipeline, param_grid, cv=5)
    clf.fit(X_TRAIN, Y_TRAIN)
    logger.info(f'Best parameters found by grid search are: {clf.best_params_}')
    logger.info(f'Best score found by grid search are: {clf.best_score_}')


def benchmark_logistic_univariate_feature_selection_script(num_cols, cat_cols, k=1):
    # Useful to  model to a simpler benchmark with few features that are easy to understand.
    # This uses SelectKBest to perform univariate analysis and pick the best features for logistic based on F-value.

    pr_logistic = get_logistic_preprocessor(num_cols, cat_cols, remainder="drop")
    pr_logistic.set_output(transform='pandas')

    X = pr_logistic.fit_transform(X_TRAIN, Y_TRAIN)

    # F-value, is ratio of explained to residuals.
    selector = SelectKBest(f_classif, k=k).fit(X, Y_TRAIN)
    selector.set_output(transform="pandas")

    selected_features = selector.get_feature_names_out().tolist()
    print(f"Selected {k} features: {selected_features}")
    f_scores_df = pd.DataFrame(
        {"feature": X.columns,
         "f_score": selector.score_func(X, Y_TRAIN)[0].tolist(),
         "p_values": selector.score_func(X, Y_TRAIN)[1].tolist()
         }
    ).sort_values(by="f_score", ascending=False)
    print(f_scores_df)

    # Doing this to select only the selected features from univariate
    bench_pipeline = Pipeline(steps=[
        ("features", pr_logistic),
        ("selector", ColumnTransformer([("selector", "passthrough", selected_features)])),
        ("logistic", LogisticRegression())]
    )

    bench_pipeline.fit(X_TEST, Y_TEST)
    return bench_pipeline


def get_gbm_preprocessor(numeric_features, cat_features):
    categorical_transformer = Pipeline(
        steps=[
            ("cabin_feature", sp.FunctionTransformer(func=cabin_feature)),
            ("encoder", sp.OneHotEncoder(handle_unknown="ignore", sparse_output=False, drop='if_binary'))
        ])

    preprocessor = ColumnTransformer([
        ('num', sp.StandardScaler(), numeric_features),
        ('cat', categorical_transformer, cat_features),
    ])
    return preprocessor.set_output(transform="pandas")


def cabin_feature(df):
    CABIN_LETTER = "cabin_letter"
    CABIN_NUMBER = "cabin_number"
    CABIN_COL = "cabin"
    # assumes for strings with multi cabins will take the first cabin and cabin number
    df[CABIN_LETTER] = df[CABIN_COL].str.strip().str.split(" ").str[0].str.get(0)
    df[CABIN_NUMBER] = df[CABIN_COL].str.strip().str.split(" ").str[0].str[1:]
    return df


def get_low_variance(df, numeric_features, categorical_features, threshold=0.2):
    # Find columns that exhibit low variance, or are constant, which could be dropped from model.
    numeric_transformer = Pipeline(
        steps=[
            ("power transform", sp.PowerTransformer(method="yeo-johnson", standardize=True)),
            ("imputer", SimpleImputer(strategy="mean"))  # important: do not use before splitting to avoid data leak
        ]
    )

    # VarianceThreshold requires categorical columns to be numeric, therefore can use Ordinal Encoder.
    label_transformer = Pipeline(steps=[("encoder", sp.OrdinalEncoder())])

    transformers = [
        ("num", numeric_transformer, numeric_features),
        ("cat", label_transformer, categorical_features)
    ]

    feature_transformer = ColumnTransformer(transformers=transformers, remainder="drop") \
        .set_output(transform="pandas")

    vt_pipeline = Pipeline(steps=[("features", feature_transformer),
                                  ("select", VarianceThreshold(threshold=threshold))]).set_output(transform="pandas")
    return list(set(feature_transformer.fit_transform(df)) - set(vt_pipeline.fit_transform(df).columns))


if __name__ == "__main__":
    xgb_train_script(save_path=os.path.join(SAVE_PATH, "my_model.json"))
